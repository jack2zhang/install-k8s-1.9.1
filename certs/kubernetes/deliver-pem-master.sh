#!/bin/bash
IP=$1
ssh root@$IP mkdir -p /etc/kubernetes/ssl
scp *.pem root@$IP:/etc/kubernetes/ssl/
#scp ca.csr root@$IP:/etc/kubernetes/ssl/
scp token.csv audit-policy.yaml root@$IP:/etc/kubernetes/

