#!/bin/bash
if ! $( command -v wget >/dev/null 2>&1 ); then
  yum install wget -y
fi
wget -c -O /tmp/cfssl http://test.wgmf.com/mirrors/cfssl/bin/cfssl
wget -c -O /tmp/cfssljson http://test.wgmf.com/mirrors/cfssl/bin/cfssljson
rm -f /usr/bin/cfssl /usr/bin/cfssljson
cp /tmp/cfssl /usr/bin/
cp /tmp/cfssljson /usr/bin/
chmod +x /usr/bin/cfssl
chmod +x /usr/bin/cfssljson
cfssl version
