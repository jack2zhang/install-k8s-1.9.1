#!/bin/bash
yum remove docker docker-common docker-selinux docker-engine -y
yum install -y yum-utils device-mapper-persistent-data lvm2
yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo
yum makecache fast
#yum list docker-ce --showduplicates | sort -r
#yum install -y docker-ce
#yum install docker-ce-18.03.0.ce-1.el7.centos
#yum install docker-ce-18.06.2.ce-3.el7 -y

# https://github.com/moby/moby/issues/33930
yum install -y --setopt=obsoletes=0 \
  docker-ce-17.03.1.ce-1.el7.centos \
  docker-ce-selinux-17.03.1.ce-1.el7.centos
systemctl start docker
yum install -y python-setuptools
easy_install pip
mkdir $HOME/.pip
cp conf/pip.conf $HOME/.pip/
pip install docker-compose
docker run hello-world
