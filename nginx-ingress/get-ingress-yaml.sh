#!/bin/bash
for file in configmap.yaml default-backend.yaml namespace.yaml rbac.yaml tcp-services-configmap.yaml udp-services-configmap.yaml with-rbac.yaml service-nodeport.yaml;
do
wget https://raw.githubusercontent.com/fungitive/kubernetes/master/ingress-nginx/$file
done
