## install-k8s 

### /etc/hosts

```
192.168.9.171 k8s-master-1
192.168.9.172 k8s-master-2
192.168.9.173 k8s-master-3
192.168.9.174 k8s-node-1
192.168.9.175 k8s-node-2
```

### hostname

```
hostnamectl --static set-hostname k8s-master-1
hostnamectl --static set-hostname k8s-master-2
hostnamectl --static set-hostname k8s-master-3
hostnamectl --static set-hostname k8s-node-1
hostnamectl --static set-hostname k8s-node-2
```

### install cfssl

### 免密

ssh-keygen -t rsa

ssh-keygen -t rsa -f ~/.ssh/id_rsa -N ''


```
ssh-copy-id 192.168.9.171
ssh-copy-id 192.168.9.172
ssh-copy-id 192.168.9.173
ssh-copy-id 192.168.9.174
ssh-copy-id 192.168.9.175
```


自动设置免密的脚本 ssh-cpoy


### install cfssl

```
yum install etcd -y
```

- 修改etcd-csr.json里面的IP


- 生成etcd的证书，分发到所有节点，包括node节点

```
./genCerts.sh 
./deliver-pem.sh 192.168.9.171
./deliver-pem.sh 192.168.9.172
./deliver-pem.sh 192.168.9.173
./deliver-pem.sh 192.168.9.174
./deliver-pem.sh 192.168.9.175

```

- 分发配置文件并修改
- 启动服务
- 检查服务状态

```
export ETCDCTL_API=3
etcdctl --cacert=/etc/etcd/ssl/etcd-root-ca.pem --cert=/etc/etcd/ssl/etcd.pem --key=/etc/etcd/ssl/etcd-key.pem --endpoints=https://192.168.18.181:2379,https://192.168.18.182:2379,https://192.168.18.183:2379 endpoint health
```
具体etcd的安装配置参考install-etcd项目
（install-etcd）https://gitee.com/jack2zhang/install-etcd


### 安装k8s的2进制文件

bin目录

- 下载

```
wget http://test.wgmf.com/mirrors/kubernetes/v191/bin/kube-apiserver 
wget http://test.wgmf.com/mirrors/kubernetes/v191/bin/kube-controller-manager 
wget http://test.wgmf.com/mirrors/kubernetes/v191/bin/kube-scheduler
wget http://test.wgmf.com/mirrors/kubernetes/v191/bin/kubectl 
wget http://test.wgmf.com/mirrors/kubernetes/v191/bin/kubelet 
wget http://test.wgmf.com/mirrors/kubernetes/v191/bin/kube-proxy

```

- 分发

```
deliver-bin-master.sh 192.168.9.171
deliver-bin-master.sh 192.168.9.172
deliver-bin-master.sh 192.168.9.173
deliver-bin-node.sh 192.168.9.174
deliver-bin-node.sh 192.168.9.175
```

### 生成和分发证书 master和node的 config配置文件

certs/kubernetes

- 生成证书
 
改kubernetes-csr.json 文件里面的IP
master和node 要分别生成 config

```
genCerts.sh		生成证书            
genTokenKubeConfig.sh 生成master的config和token
set-kubelet-config.sh 生成node的config
```

- 分发证书

```
./deliver-pem-master.sh 192.168.9.171
./deliver-pem-master.sh 192.168.9.172
./deliver-pem-master.sh 192.168.9.173
./deliver-pem-node.sh 192.168.9.174
./deliver-pem-node.sh 192.168.9.175

```

### 分发systemd 文件

- 分发完需要改IP


```
./deliver-systemd-master.sh 192.168.9.171
./deliver-systemd-master.sh 192.168.9.172
./deliver-systemd-master.sh 192.168.9.173
```

- master启动

kube-apiserver.service
kube-controller-manager
kube-scheduler.service

```
start-systemd-master.sh
```
- master组件检查

```
kubectl get cs
```

### node 安装 docker

docker/

```
install_docker4centos7.sh
```

### node 安装 nginx-proxy

nginx.conf改成master的IP

部署

```
deploy-nginx.sh 192.168.9.174
deploy-nginx.sh 192.168.9.175

```

### node 安装 kube组件

master上执行授权 创建请求证书
```
kubectl create clusterrolebinding kubelet-bootstrap --clusterrole=system:node-bootstrapper --user=kubelet-bootstrap
```

- 分发node的systemd

# 此处重复了

```
./deliver-systemd-node.sh 192.168.18.174
./deliver-systemd-node.sh 192.168.18.175
```

改IP 和 hostname 并启动

```
/deliver-systemd-node.sh 192.168.18.174
/deliver-systemd-node.sh 192.168.18.175
```
systemd路径

/etc/systemd/system/

改IP和hostname

下载基础镜像

docker-wrapper
./docker-wrapper.py pull gcr.io/google_containers/pause-amd64:3.0

- 安装ipvs

```
yum install ipvsadm -y

```


启动

```
systemctl start kubelet.service
systemctl enable kubelet.service
systemctl start kube-proxy.service
systemctl enable kube-proxy.service
```

- 限制node的调度

```
kubectl cordon  k8s-node-1
kubectl uncordon  k8s-node-1
```

- master配置认证

kubectl get csr

```
kubectl get csr | grep Pending | awk '{print $1}' | xargs kubectl certificate approve
```

kubectl get nodes

### Flannel 


docker conf

```
mv /usr/lib/systemd/system/docker.service.d/flannel.conf /etc/systemd/system/docker.service.d

```

set-etcdctl-flannel

```
etcdctl --endpoints=https://192.168.18.171:2379,https://192.168.18.172:2379,https://192.168.18.173:2379 --cert-file=/etc/etcd/ssl/etcd.pem --ca-file=/etc/etcd/ssl/etcd-root-ca.pem --key-file=/etc/etcd/ssl/etcd-key.pem set /flannel/network/config \ '{"Network":"10.254.64.0/18","SubnetLen":24,"Backend":{"Type":"host-gw"}}'

```

/etc/sysconfig/flanneld

```
FLANNEL_ETCD_ENDPOINTS="https://192.168.18.171:2379,https://192.168.18.172:2379,https://192.168.18.173:2379"

FLANNEL_ETCD_PREFIX="/flannel/network"

FLANNEL_OPTIONS="-ip-masq=true -etcd-cafile=/etc/etcd/ssl/etcd-root-ca.pem -etcd-certfile=/etc/etcd/ssl/etcd.pem -etcd-keyfile=/etc/etcd/ssl/etcd-key.pem -iface=eth0"
```

重新设置docker

docker/
set-docker-kubernetes.sh

### 安装helm

* node

```
yum install socat -y
docker-wrapp️er.py pull gcr.io/kubernetes-helm/tiller:v2.14.0 

```

* master

install-helm.sh

### coredns

- install coredns

coredns/
exam.sh
./deploy.sh -r 10.254.0.0/18 -i 10.254.0.2 | kubectl apply -f -

- 查看coredns的POD

```
kubectl get pods -n kube-system

```
- test coredns

testcore/
kubectl apply -f alpine.yaml 

test

```
kubectl get pods,svc 
kubectl exec -it alpine nslookup nginx-svc
kubectl exec -it alpine nslookup kubernetes
```

### ISSUE

- 生成和同步etcd.conf的配置

