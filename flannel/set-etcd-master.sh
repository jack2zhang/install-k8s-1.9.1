export export ETCDCTL_API=2
etcdctl --endpoints=https://192.168.18.151:2379,https://192.168.18.152:2379,https://192.168.18.153:2379 --cert-file=/etc/etcd/ssl/etcd.pem --ca-file=/etc/etcd/ssl/etcd-root-ca.pem --key-file=/etc/etcd/ssl/etcd-key.pem set /flannel/network/config \ '{"Network":"10.254.64.0/18","SubnetLen":24,"Backend":{"Type":"host-gw"}}'

