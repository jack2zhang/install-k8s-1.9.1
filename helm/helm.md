## helm

- 在每个node上安装

```
yum install -y socat

```

国外下载
```
curl https://raw.githubusercontent.com/kubernetes/helm/master/scripts/get > get_helm.sh
chmod 700 get_helm.sh
./get_helm.sh
```

自己软件仓库下载

```
wget -c http://test.wgmf.com/mirrors/k8s/helm/src/helm-v2.14.0-linux-amd64.tar.gz
tar zxvf helm-v2.14.0-linux-amd64.tar.gz
cp linux-amd64/helm /usr/local/bin/

```

创建tiller的serviceaccount和clusterrolebinding
然后安装helm服务端tiller

```
kubectl create serviceaccount --namespace kube-system tiller
kubectl create clusterrolebinding tiller-cluster-rule --clusterrole=cluster-admin --serviceaccount=kube-system:tiller
kubectl patch deploy --namespace kube-system tiller-deploy -p '{"spec":{"template":{"spec":{"serviceAccount":"tiller"}}}}'

```

- 提前下载好镜像

```
docker-wrapp️er.py pull gcr.io/kubernetes-helm/tiller:v2.14.0 
```

- 安装Tiller

```
helm init --skip-refresh

```

- 更换国内源

```
helm repo remove stable
helm repo add stable https://kubernetes.oss-cn-hangzhou.aliyuncs.com/charts
helm repo update
helm search
helm repo list 
```

-  验证

```
helm version
Client: &version.Version{SemVer:"v2.14.0", GitCommit:"05811b84a3f93603dd6c2fcfe57944dfa7ab7fd0", GitTreeState:"clean"}
Server: &version.Version{SemVer:"v2.14.0", GitCommit:"05811b84a3f93603dd6c2fcfe57944dfa7ab7fd0", GitTreeState:"clean"}

```

如果是用自己的镜像
替换为自己的镜像    

```
helm init --upgrade --tiller-image=jicki/tiller:v2.11.0
helm init --upgrade -i jicki/tiller:v2.11.0

```


删除tiller
```
helm reset -f
rm -rf /root/.helm
```

排错
```
kubectl get pods,svc -n kube-system
kubectl logs tiller-deploy-77bf945dd-b4zr4 -n kube-system
kubectl describe pod tiller-deploy-77bf945dd-b4zr4 -n kube-system

```

FAQ:
Q:socat not found.
A: yum install -y socat


#### ref

https://jicki.me/kubernetes/docker/2018/12/07/helm/

https://jimmysong.io/posts/manage-kubernetes-native-app-with-helm/
