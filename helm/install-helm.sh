#/bin/bash
kubectl create serviceaccount --namespace kube-system tiller
kubectl create clusterrolebinding tiller-cluster-rule --clusterrole=cluster-admin --serviceaccount=kube-system:tiller
kubectl patch deploy --namespace kube-system tiller-deploy -p '{"spec":{"template":{"spec":{"serviceAccount":"tiller"}}}}'
# pull images
docker-wrapp️er.py pull gcr.io/kubernetes-helm/tiller:v2.14.0
# install tiller
helm init --skip-refresh
# change mirrors
helm repo remove stable
helm repo add stable https://kubernetes.oss-cn-hangzhou.aliyuncs.com/charts
helm repo update
helm search
helm repo list 
# version
helm version

