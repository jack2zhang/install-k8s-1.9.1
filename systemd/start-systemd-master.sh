service=(
kube-apiserver.service
kube-controller-manager
kube-scheduler.service
)
for service in ${service[@]};do
echo "starting" $service
systemctl start $service
systemctl enable $service
done

